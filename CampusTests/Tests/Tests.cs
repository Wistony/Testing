using NUnit.Framework;

namespace CampusTests
{
    using System;
    using System.Threading;
    using OpenQA.Selenium;

    public class Tests
    {
        private IWebDriver driver;

        private const string Url = "https://ecampus.kpi.ua/home";

        private const string login = "amv76398";
        private const string password = "qwerty";

        [SetUp]
        public void Setup()
        {
            driver = new OpenQA.Selenium.Chrome.ChromeDriver();
            driver.Navigate().GoToUrl(Url);
            driver.Manage().Window.Maximize();
            var authPage = new AuthorizationPage(driver);
            
            authPage.SetLogin(login);
            authPage.SetPassword(password);
           
            var homePage = authPage.ClickLoginButton();
            Thread.Sleep(3000);
            homePage.ClickOnCurrentVersionButton();
            Thread.Sleep(3000);
        }

        [Test]
        public void FullNameEngFieldTest()
        {
            var homePage = new OldHomePage(driver);
            var profilePage = homePage.ClickOnMyProfile();
            
            Thread.Sleep(1000);
            
            profilePage.ClickOnEditNameButton();

            var fullName = "Maksym Androshchuk";
            
            profilePage.ClearFullNameField();
            profilePage.SetFullName(fullName);
            profilePage.ClickOnSaveButton();

            var actualResult = profilePage.GetAttributeFromFullnameField("value");
            Assert.AreEqual(fullName,actualResult);
        }
        
        [Test]
        public void CuratorPageTest()
        {
            var homePage = new OldHomePage(driver);
            var curatorPage = homePage.ClickOnCuratorMenuItem();
            
            Thread.Sleep(3000);
            
            var personalCuratorPage = curatorPage.ClickOnPersonalCuratorPage();

            Thread.Sleep(3000);

            var titleText = personalCuratorPage.GetTitleText();
            var unexpectedResult = "Not Found";
            Assert.AreNotEqual(unexpectedResult, titleText,"Page return code 404");
        }
        
        [Test]
        public void ExitButtonTest()
        {
            var homePage = new OldHomePage(driver);
            homePage.ClickOnExitButton();

            var actualUrl = driver.Url;
            
            Assert.AreEqual(actualUrl,Url); 
        }
    }
}