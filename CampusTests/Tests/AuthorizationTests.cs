using NUnit.Framework;

namespace CampusTests
{
    using System.Threading;
    using OpenQA.Selenium;

    public class AuthorizationTests
    {
        private IWebDriver driver;

        private const string Url = "https://ecampus.kpi.ua/home";

        private const string login = "amv76398";
        private const string password = "qwerty";
        private const string wrongPassword = "abcd1234";


        [SetUp]
        public void Setup()
        {
            driver = new OpenQA.Selenium.Chrome.ChromeDriver();
            driver.Navigate().GoToUrl(Url);
            driver.Manage().Window.Maximize();
        }
        
        [Test]
        public void SuccessfulAuthorizationTest()
        {
            var authPage = new AuthorizationPage(driver);
            
            authPage.SetLogin(login);
            authPage.SetPassword(password);
           
            var homePage = authPage.ClickLoginButton();
            
            Thread.Sleep(3000);
            
            homePage.ClickOnAccountDropdownMenu();
            var settingsPage = homePage.ClickOnSettings();

            var actualValue = settingsPage.GetLoginName();
            
            Assert.AreEqual(login,actualValue);
        }
        
        [Test]
        public void AuthorizationWithWrongPasswordTest()
        {
            var authPage = new AuthorizationPage(driver);
            
            authPage.SetLogin(login);
            authPage.SetPassword(wrongPassword);

            authPage.ClickLoginButton();
            
            Thread.Sleep(1000);
            var alertMessage = authPage.GetAlertMessage();

            var expectedMessage = "×\r\nПеревірте корректність логіну та паролю.";
            
            Assert.AreEqual(alertMessage, expectedMessage);
        }
    }
}