namespace CampusTests
{
    using OpenQA.Selenium;

    public class ProfilePage
    {
        private IWebDriver driver;
        
        private readonly By editNameBtn = By.CssSelector("a.image-link-edit");
        private readonly By fullNameEngField = By.Name("FullNameEng");
        private readonly By searchBtn = By.Id("search_btn");
        private readonly By saveButton = By.CssSelector("input.image-link-save");
        private readonly By field = By.CssSelector("input.none.text-field");

        public ProfilePage(IWebDriver driver)
        {
            this.driver = driver;
        }

        public void ClickOnEditNameButton()
        {
            driver.FindElement(editNameBtn).Click();
        }

        public void ClearFullNameField()
        {
            driver.FindElement(fullNameEngField).Clear();
        }
        
        public void SetFullName(string fullname)
        {
            driver.FindElement(fullNameEngField).SendKeys(fullname);
        }

        public void ClickOnSaveButton()
        {
            driver.FindElement(saveButton).Click();
        }

        public string GetAttributeFromFullnameField(string attribute)
        {
            return driver.FindElement(field).GetAttribute(attribute);
        }
        
    }
}