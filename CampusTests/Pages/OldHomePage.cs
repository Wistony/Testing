namespace CampusTests
{
    using OpenQA.Selenium;

    public class OldHomePage
    {
        private IWebDriver driver;

        private readonly By myProfileMenu = By.LinkText("Мій профіль");
        private readonly By exitButton = By.CssSelector("img[title='Вихід']");
        private readonly By curatorMenu = By.LinkText("Куратор");


        public OldHomePage(IWebDriver driver)
        {
            this.driver = driver;
        }

        public ProfilePage ClickOnMyProfile()
        {
            driver.FindElement(myProfileMenu).Click();
            return new ProfilePage(driver);
        }

        public NewHomePage ClickOnExitButton()
        {
            driver.FindElement(exitButton).Click();
            return new NewHomePage(driver);
        }

        public CuratorPage ClickOnCuratorMenuItem()
        {
            driver.FindElement(curatorMenu).Click();
            return new CuratorPage(driver);
        }


    }
}