namespace CampusTests
{
    using OpenQA.Selenium;

    public class PersonalCuratorPage
    {
        private IWebDriver driver;
        
        private readonly By title = By.CssSelector("h1");

        public PersonalCuratorPage(IWebDriver driver)
        {
            this.driver = driver;
        }

        public string GetTitleText()
        {
            return driver.FindElement(title).Text;
        }
        
    }
}