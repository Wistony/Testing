namespace CampusTests
{
    using OpenQA.Selenium;

    public class AuthorizationPage
    {
        private IWebDriver driver;

        public AuthorizationPage(IWebDriver driver)
        {
            this.driver = driver;
        }
        
        private readonly By loginField = By.XPath("//input[@class = 'form-control'][@placeholder = 'Логін']");
        private readonly By passwordField = By.XPath("//input[@class = 'form-control'][@placeholder = 'Пароль']");
        private readonly By loginButton = By.XPath("//input[@class = 'btn btn-success btn-block'][@value = 'Вхід']");
        
        private readonly By alert = By.CssSelector("div.alert.alert-danger");

        public void SetLogin(string login)
        {
            driver.FindElement(loginField).SendKeys(login);
        }

        public void SetPassword(string password)
        {
            driver.FindElement(passwordField).SendKeys(password);
        }

        public NewHomePage ClickLoginButton()
        {
            driver.FindElement(loginButton).Click();
            return new NewHomePage(driver);
        }
        
        public string GetAlertMessage()
        {
            return driver.FindElement(alert).Text;
        }
    }
}