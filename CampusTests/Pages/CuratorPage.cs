namespace CampusTests
{
    using OpenQA.Selenium;

    public class CuratorPage
    {
        private IWebDriver driver;
        
        private readonly By curatorPage = By.CssSelector("a.kurator_publish_URL_link");

        public CuratorPage(IWebDriver driver)
        {
            this.driver = driver;
        }

        public PersonalCuratorPage ClickOnPersonalCuratorPage()
        {
            driver.FindElement(curatorPage).Click();
            return new PersonalCuratorPage(driver);
        }
        
    }
}