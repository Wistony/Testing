namespace CampusTests
{
    using OpenQA.Selenium;

    public class NewHomePage
    {
        private IWebDriver driver;

        public NewHomePage(IWebDriver driver)
        {
            this.driver = driver;
        }
        
        private readonly By accountDropDownMenu = By.XPath("//a[@class = 'dropdown-toggle nav-link'][text()='Аккаунт']");
        private readonly By linkToSettings = By.XPath("//a[@class = 'dropdown-item'][@href= '/settings']");
        private readonly By currentVersionBtn =
            By.XPath("//button[@class = 'btn btn-primary btn-lg'][text() = 'До поточної версії кампусу']");

        public void ClickOnAccountDropdownMenu()
        {
            driver.FindElement(accountDropDownMenu).Click();
        }
        public OldHomePage ClickOnCurrentVersionButton()
        {
            driver.FindElement(currentVersionBtn).Click();
            return new OldHomePage(driver);
        }

        public SettingsPage ClickOnSettings()
        {
            driver.FindElement(linkToSettings).Click();
            return new SettingsPage(driver);
        }
    }
}