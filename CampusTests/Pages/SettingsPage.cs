namespace CampusTests
{
    using OpenQA.Selenium;

    public class SettingsPage
    {
        private IWebDriver driver;
        
        private readonly By loginName = By.XPath("//div[@class = 'profile-head']/span[2]");

        public SettingsPage(IWebDriver driver)
        {
            this.driver = driver;
        }

        public string GetLoginName()
        {
            return driver.FindElement(loginName).Text;
        }
        
        
    }
}